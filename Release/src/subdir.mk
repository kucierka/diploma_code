################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/all_sat_index.cpp \
../src/backtrack.cpp \
../src/bubble_coloring.cpp \
../src/coloring.cpp \
../src/convert_graph.cpp \
../src/digraph.cpp \
../src/functions.cpp \
../src/graph_structure.cpp \
../src/main.cpp \
../src/sat_instance.cpp \
../src/sat_instance_edges.cpp \
../src/sat_instance_vertices.cpp \
../src/simple_path.cpp \
../src/simple_sat_solution.cpp \
../src/solution.cpp \
../src/solution_four_cut.cpp \
../src/solution_four_cut_memo.cpp \
../src/tight_cycle_enumeration.cpp \
../src/tight_cycle_solution.cpp 

OBJS += \
./src/all_sat_index.o \
./src/backtrack.o \
./src/bubble_coloring.o \
./src/coloring.o \
./src/convert_graph.o \
./src/digraph.o \
./src/functions.o \
./src/graph_structure.o \
./src/main.o \
./src/sat_instance.o \
./src/sat_instance_edges.o \
./src/sat_instance_vertices.o \
./src/simple_path.o \
./src/simple_sat_solution.o \
./src/solution.o \
./src/solution_four_cut.o \
./src/solution_four_cut_memo.o \
./src/tight_cycle_enumeration.o \
./src/tight_cycle_solution.o 

CPP_DEPS += \
./src/all_sat_index.d \
./src/backtrack.d \
./src/bubble_coloring.d \
./src/coloring.d \
./src/convert_graph.d \
./src/digraph.d \
./src/functions.d \
./src/graph_structure.d \
./src/main.d \
./src/sat_instance.d \
./src/sat_instance_edges.d \
./src/sat_instance_vertices.d \
./src/simple_path.d \
./src/simple_sat_solution.d \
./src/solution.d \
./src/solution_four_cut.d \
./src/solution_four_cut_memo.d \
./src/tight_cycle_enumeration.d \
./src/tight_cycle_solution.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -g -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


