/* This program gets canonical graph
   This version uses sparse form with dynamic allocation.
*/

#include "nausparse.h"    /* which includes nauty.h */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define BUFFER 256

static char* getline_from_file(FILE *f) {
	long long i = 0;
	char *line = malloc(0);
	int c;
	int pos = 0;
	while((c = getc(f)) != EOF && c != '\n') {
		char *tmp = realloc(line, (pos+1)*sizeof(line));
		line = tmp;
		line[pos] = ((char)c);
		pos++;
	}

	if (i == 0 && c == EOF) {
		line = "";
	}
	return line;
}

int main(int argc_, char *argv_[]) {
	DYNALLSTAT(int,lab,lab_sz);
	DYNALLSTAT(int,ptn,ptn_sz);
	DYNALLSTAT(int,orbits,orbits_sz);
	DYNALLSTAT(int,map,map_sz);
    static DEFAULTOPTIONS_SPARSEGRAPH(options);
    statsblk stats;

    /* Declare and initialize sparse graph structures */
    SG_DECL(sg);
    SG_DECL(cg);

    /* Select option for canonical labelling */
    options.getcanon = TRUE;

    int argc = argc_;
    if (argc <= 2) {
    	fprintf(stderr, ">E canonize: too few arguments.\n");
    	exit(1);
    }

    FILE *ifp;
    ifp = fopen(argv_[1], "r");
    if (ifp == NULL) {
    	fprintf(stderr, ">E canonize: can't open %s\n", argv_[1]);
    	exit(1);
    }

    char* line;
    long long n, m;
    if ((line=getline_from_file(ifp)) != "") {
    	n = atoi(line);
    	line = strchr(line, ' ');
    	m = atoi(line)*2;
    }

    int mm,i;

    mm = SETWORDSNEEDED(n);
    nauty_check(WORDSIZE,mm,n,NAUTYVERSIONID);

    DYNALLOC1(int,lab,lab_sz,n,"malloc");
    DYNALLOC1(int,ptn,ptn_sz,n,"malloc");
    DYNALLOC1(int,orbits,orbits_sz,n,"malloc");
    DYNALLOC1(int,map,map_sz,n,"malloc");

    /* Now make the graph */
    SG_ALLOC(sg,n,m,"malloc");
    sg.nv = n;			/* Number of vertices */
    sg.nde = m;		/* Number of directed edges */

    int pos = 0;

    for (i = 0; i < n; ++i) {
    	sg.v[i] = pos;
    	int j = 0;
    	line = getline_from_file(ifp);
    	long long u = atoi(line);
    	sg.e[pos+j] = u;
    	j++;
    	while((line = strchr(line, ' ')) != NULL) {
    		line = strcpy(line, line+1);
    		u = atoi(line);
    		sg.e[pos+j] = u;
    		j++;
    	}
    	sg.d[i] = j;
    	pos += j;
    }

    fclose(ifp);

    /* Label sg, result in cg and labelling in lab;
     * It is not necessary to pre-allocate space in cg, but
     * it has to be initialised as we did above.  */
    sparsenauty(&sg,lab,ptn,orbits,&options,&stats,&cg);

    FILE *ofp;
    ofp = fopen(argv_[2], "w");
    if (ofp == NULL) {
    	fprintf(stderr, ">E canonize: can't open output file %s\n",
    			argv_[2]);
    	exit(1);
    }

    fprintf(ofp, "%lld %lld\n", n, m);
    for (i = 0; i < n; ++i) {
    	fprintf(ofp, "%d", cg.e[cg.v[i]]);
    	for (int j = 1; j < cg.d[i]; ++j) {
    		fprintf(ofp, " %d", cg.e[cg.v[i]+j]);
    	}
    	fprintf(ofp, "\n");
    }
    fclose(ofp);

    return 0;
}
